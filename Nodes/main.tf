provider "azurerm" {
  # whilst the `version` attribute is optional, we recommend pinning to a given version of the Provider
  version         = "=1.36.1"
  subscription_id = "${var.az_subscription_id}"
  tenant_id       = "${var.az_tenant_id}"
}

provider "azuread" {
  version         = "=0.6.0"
  subscription_id = "${var.az_subscription_id}"
  tenant_id       = "${var.az_tenant_id}"
}

# Create a resource group
data "azurerm_resource_group" "docker-rg" {
  name     = "${var.az_rg_name}"
}

data "azurerm_virtual_network" "docker-vnet" {
  name                = "${var.az_vnet_name}"
  resource_group_name = "${data.azurerm_resource_group.docker-rg.name}"
}

data "azurerm_subnet" "docker-snet" {
  name                 = "${var.az_subnet_name}"
  resource_group_name  = "${data.azurerm_resource_group.docker-rg.name}"
  virtual_network_name = "${data.azurerm_virtual_network.docker-vnet.name}"
}

data "azurerm_public_ip" "docker-jb-ip" {
    name                = "${var.az_jb_pubip}"
    resource_group_name = "${data.azurerm_resource_group.docker-rg.name}"
}

data "azuread_application" "docker-app" {
  name = "${var.docker_app_name}"
}

data "template_file" "ssh_config_host" {
  template = "${file("../_files/ssh_config_host.tmpl")}"
  count    = "${length(var.docker_nodes)}"

  vars = {
    host = "${lookup(var.docker_nodes, count.index)}"
    ip   = "${element(azurerm_network_interface.docker-node-nic.*.private_ip_address, count.index)}"
  }
}

data "template_file" "ssh_config" {
  template = "${file("../_files/ssh_config.tmpl")}"

  vars = {
    ssh_user = "${var.ssh_user}"
    hosts    = "${join("\n", data.template_file.ssh_config_host.*.rendered)}"
  }
}

data "template_file" "azure_json" {
  template = "${file("../_files/azure.json")}"

  vars = {
    az_tenant       = "${var.az_tenant_id}"
    az_subscription = "${var.az_subscription_id}"
    az_app          = "${data.azuread_application.docker-app.application_id}"
    az_secret       = "${var.docker_sp_secret}"
    az_rg           = "${var.az_rg_name}"
    az_loc          = "${var.az_location}"
    az_subnet       = "${var.az_subnet_name}"
    az_vnet         = "${var.az_vnet_name}"
    az_nsg          = "${var.az_vm_nsg}"
  }
}
