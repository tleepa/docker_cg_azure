variable "az_tenant_id" {}
variable "az_subscription_id" {}
variable "az_location" {}
variable "az_rg_name" {
  default = "docker-rg"
}
variable "az_vnet_name" {
  default = "docker-vnet"
}
variable "az_subnet_name" {
  default = "docker-subnet"
}
variable "az_vnet_cidr" {
  default = "172.16.0.0/16"
}
variable "az_subnet_cidr" {
  default = "172.16.0.0/16"
}
variable "az_jb_nsg" {
  default = "docker-jb-nsg"
}
variable "az_jb_dnshostname" {}
variable "az_jb_pubip" {
  default = "docker-jb-ip"
}
variable "az_vm_nsg" {
  default = "docker-vm-nsg"
}
variable "az_vm_size" {
  default = "Standard_B2s"
}
variable "ssh_user" {}
variable "ssh_private_key_path" {}
variable "ssh_public_key_path" {}
variable "docker_url" {}
variable "docker_app_name" {}
variable "docker_sp_secret" {}
