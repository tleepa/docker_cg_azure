provider "azurerm" {
  # whilst the `version` attribute is optional, we recommend pinning to a given version of the Provider
  version         = "=1.36.1"
  subscription_id = "${var.az_subscription_id}"
  tenant_id       = "${var.az_tenant_id}"
}

provider "azuread" {
  version         = "=0.6.0"
  subscription_id = "${var.az_subscription_id}"
  tenant_id       = "${var.az_tenant_id}"
}
# Create a resource group
data "azurerm_subscription" "current" {}

resource "azurerm_resource_group" "docker-rg" {
  name     = "${var.az_rg_name}"
  location = "${var.az_location}"
}

resource "azurerm_virtual_network" "docker-vnet" {
  name                = "${var.az_vnet_name}"
  resource_group_name = "${azurerm_resource_group.docker-rg.name}"
  location            = "${azurerm_resource_group.docker-rg.location}"
  address_space       = ["${var.az_vnet_cidr}"]
}

resource "azurerm_subnet" "docker-snet" {
  name                 = "${var.az_subnet_name}"
  resource_group_name  = "${azurerm_resource_group.docker-rg.name}"
  virtual_network_name = "${azurerm_virtual_network.docker-vnet.name}"
  address_prefix       = "${var.az_subnet_cidr}"
}

resource "azurerm_network_security_group" "docker-jb-nsg" {
  name                = "${var.az_jb_nsg}"
  location            = "${azurerm_resource_group.docker-rg.location}"
  resource_group_name = "${azurerm_resource_group.docker-rg.name}"

  security_rule {
    name                       = "SSH"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

resource "azurerm_network_security_group" "docker-vm-nsg" {
  name                = "${var.az_vm_nsg}"
  location            = "${azurerm_resource_group.docker-rg.location}"
  resource_group_name = "${azurerm_resource_group.docker-rg.name}"

  security_rule {
    name                       = "SSH"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

resource "azurerm_subnet_network_security_group_association" "docker-snet-nsg" {
  subnet_id                 = "${azurerm_subnet.docker-snet.id}"
  network_security_group_id = "${azurerm_network_security_group.docker-vm-nsg.id}"
}

resource "azuread_application" "docker-app" {
  name            = "${var.docker_app_name}"
  identifier_uris = ["https://${var.docker_app_name}"]

  # required_resource_access {
  #   resource_app_id = "00000003-0000-0000-c000-000000000000"

  #   resource_access {
  #     id   = "e1fe6dd8-ba31-4d61-89e7-88639da4683d"
  #     type = "Scope"
  #   }
  # }
}

resource "azuread_service_principal" "docker-sp" {
  application_id = "${azuread_application.docker-app.application_id}"
}

resource "azuread_service_principal_password" "docker-secret" {
  service_principal_id = "${azuread_service_principal.docker-sp.id}"
  value                = "${var.docker_sp_secret}"
  end_date             = "2020-01-01T01:02:03Z"
}

resource "azurerm_role_assignment" "docker-app-contrib" {
  scope                = "${data.azurerm_subscription.current.id}"
  role_definition_name = "Contributor"
  principal_id         = "${azuread_service_principal.docker-sp.id}"
}
