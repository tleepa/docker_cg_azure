resource "azurerm_public_ip" "docker-jb-ip" {
    name                = "docker-jb-ip"
    location            = "${azurerm_resource_group.docker-rg.location}"
    resource_group_name = "${azurerm_resource_group.docker-rg.name}"
    domain_name_label   = "${var.az_jb_dnshostname}"
    allocation_method   = "Dynamic"
}

resource "azurerm_network_interface" "docker-jb-nic" {
  name                      = "docker-jb-nic"
  location                  = "${azurerm_resource_group.docker-rg.location}"
  resource_group_name       = "${azurerm_resource_group.docker-rg.name}"
  network_security_group_id = "${azurerm_network_security_group.docker-jb-nsg.id}"

  ip_configuration {
    name                          = "docker-jb-ipconfig"
    subnet_id                     = "${azurerm_subnet.docker-snet.id}"
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = "${azurerm_public_ip.docker-jb-ip.id}"
  }
}

resource "azurerm_virtual_machine" "docker-jb-vm" {
  name                  = "docker-jb-vm"
  location              = "${azurerm_resource_group.docker-rg.location}"
  resource_group_name   = "${azurerm_resource_group.docker-rg.name}"
  network_interface_ids = ["${azurerm_network_interface.docker-jb-nic.id}"]
  vm_size               = "Standard_B1ms"

  # Uncomment this line to delete the OS disk automatically when deleting the VM
  delete_os_disk_on_termination = true

  # Uncomment this line to delete the data disks automatically when deleting the VM
  delete_data_disks_on_termination = true

  storage_image_reference {
    publisher = "OpenLogic"
    offer     = "CentOS"
    sku       = "7.6"
    version   = "latest"
  }

  storage_os_disk {
    name              = "docker-jb-osdisk1"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "StandardSSD_LRS"
  }

  os_profile {
    computer_name  = "docker-jb"
    admin_username = "${var.ssh_user}"
  }

  os_profile_linux_config {
    disable_password_authentication = true
    ssh_keys {
      path     = "/home/${var.ssh_user}/.ssh/authorized_keys"
      key_data = "${file(var.ssh_public_key_path)}"
    }
  }

  provisioner "file" {
    source      = "${var.ssh_private_key_path}"
    destination = "/home/${var.ssh_user}/.ssh/id_rsa"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod 600 /home/${var.ssh_user}/.ssh/id_rsa",
    ]
  }

  connection {
    type        = "ssh"
    host        = "${azurerm_public_ip.docker-jb-ip.fqdn}"
    user        = "${var.ssh_user}"
    private_key = "${file(var.ssh_private_key_path)}"
  }
}

output "jb_connection_string" {
  value = "ssh -i ${abspath(var.ssh_private_key_path)} ${var.ssh_user}@${azurerm_public_ip.docker-jb-ip.fqdn}"
}
