#!/bin/bash

# sudo yum update -y

sudo yum install -y epel-release
sudo yum install -y bind-utils wget net-tools nmap-ncat
sudo yum install -y yum-utils device-mapper-persistent-data lvm2

export DOCKERURL="${docker-ee-url}"
sudo -E sh -c 'echo "$DOCKERURL/centos" > /etc/yum/vars/dockerurl'
sudo -E yum-config-manager --add-repo "$DOCKERURL/centos/docker-ee.repo"

sudo yum -y install docker-ee docker-ee-cli containerd.io

sudo systemctl enable docker
sudo systemctl start docker

groupadd docker
usermod -aG docker ${username}
