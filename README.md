# Docker EE on Azure

This repo uses Terraform to create Docker EE infrastructure on Azure to help with CG workshops.

## Overview

This is a 2-step deployment that will let you more easily destroy and redeploy docker nodes only
when something goes wrong.

In your Azure subscription:

1. First step will create:
   - Resource Group
   - Virtual Network
   - Subnet
   - Network Security Group (for Jumpbox/Bastion host)
   - Public IP (for Jumpbox/Bastion host)
   - Network Interface Card (for Jumpbox/Bastion host)
   - Jumpbox/Bastion host

    And will output ssh commmand to connect to created Jumpbox/Bastion host using fqdn.

2. Second step will create:
   - 3 docker nodes:
     - ucp (universal control plane)
     - dtr (trusted registry)
     - wrk (worker)

    And will bootstrap them with [bootstrap script](_files/bootstrap.sh) that will install
    required packages and start docker service.

Notes:

- All 4 VMs use latest CentOS image and StandardSSD_LRS storage tier.
- Bastion VM uses Standard_B1ms size.
- Docker nodes use Standard_B2s size.
- Only port 22 is open from Internet - only to Jumpbox/Bastion host.

## Requirements

You have to have:

- Azure account (know your _<tenant_id>_ and _<subscription_id>_)
- [Docker EE trial](https://hub.docker.com/editions/enterprise/docker-ee-trial) access
- SSH key generated (private and public keys)
- [azure-cli](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli?view=azure-cli-latest) installed
- [terraform](https://www.terraform.io/downloads.html) installed

## Actions

These steps were tested on Windows.

1. Setup (in the command prompt):
    - login to Azure: `az login`
    - clone this repo: `git clone https://gitlab.com/tleepa/docker_cg_azure.git`
    - `cd docker_cg_azure`

2. Create `terraform.tfvars` file in this directory with following ocntent:

    ```terraform
    az_subscription_id = "<subscription_id>"
    az_tenant_id = "<tenant_id>"
    az_location = "<location_name>"
    az_jb_dnshostname = "<jumpbox_hostname>"
    ssh_user = "<ssh_user>"
    ssh_private_key_path = "<path_to_ssh_private_key>"
    ssh_public_key_path = "<path_to_ssh_public_key>"
    docker_url = "<docker_url>"
    ```

    where:

    - _<subscription_id>_, _<tenant_id>_, _<location_name>_ are your Azure details (you can find locations using `az account list-locations -o table`)
    - _<jumpbox_hostname>_ is the pulic hostname that will be assigned to the jumpbox public IP address (like  _<jumpbox_hostname>_.westeurope.cloudapp.azure.com).
    - _<ssh_user>_ is the name of the user that will be provisioned on the VMs (bastion and docker nodes)
    - _<docker_url>_ is the storebits setup URL (like https://storebits.docker.com/ee/trial/sub-<some_uuid>) taken from Docker EE trial setup page:
      ![Setup](docker_ee_trial.png)

3. Create infrastructure:
    - `cd Infra`
    - `terraform init`
    - `terraform validate`
    - `terraform apply -var-file ../terraform.tfvars`

4. Create docker nodes:
    - `cd ../Nodes`
    - `terraform init`
    - `terraform validate`
    - `terraform apply -var-file ../terraform.tfvars`

5. Play:
    - Login to bastion host over SSH - command is output from "Infra" step.
    - From bastion host, use `ssh <node_name>` (e.g. `ssh ucp`) to connect to provisioned docker host - <node_name> comes from `docker_nodes` variable.
    - Do your docker stuff :)

6. When done, you can destroy either docker nodes only, or everything:
    - `cd Nodes`
    - `terraform destroy -var-file ../terraform.tfvars`
    - `cd ../Infra`
    - `terraform destroy -var-file ../terraform.tfvars`

Have fun!
