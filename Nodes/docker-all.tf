variable "docker_nodes" {
   default = {
    "0" = "ucp"
    "1" = "dtr"
    "2" = "wkr"
  }
}

resource "azurerm_network_interface" "docker-node-nic" {
  count               = "${length(var.docker_nodes)}"
  name                = "docker-${lookup(var.docker_nodes, count.index)}-nic"
  location            = "${data.azurerm_resource_group.docker-rg.location}"
  resource_group_name = "${data.azurerm_resource_group.docker-rg.name}"

  ip_configuration {
    name                          = "docker-${lookup(var.docker_nodes, count.index)}-ipconfig"
    subnet_id                     = "${data.azurerm_subnet.docker-snet.id}"
    private_ip_address_allocation = "Dynamic"
  }
}

resource "azurerm_virtual_machine" "docker-node-vm" {
  count                 = "${length(var.docker_nodes)}"
  name                  = "docker-${lookup(var.docker_nodes, count.index)}-vm"
  location              = "${data.azurerm_resource_group.docker-rg.location}"
  resource_group_name   = "${data.azurerm_resource_group.docker-rg.name}"
  network_interface_ids = ["${element(azurerm_network_interface.docker-node-nic.*.id, count.index)}"]
  vm_size               = "${var.az_vm_size}"

  # Uncomment this line to delete the OS disk automatically when deleting the VM
  delete_os_disk_on_termination = true

  # Uncomment this line to delete the data disks automatically when deleting the VM
  delete_data_disks_on_termination = true

  storage_image_reference {
    publisher = "OpenLogic"
    offer     = "CentOS"
    sku       = "7.6"
    version   = "latest"
  }

  storage_os_disk {
    name              = "docker-${lookup(var.docker_nodes, count.index)}-osdisk1"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "StandardSSD_LRS"
  }

  os_profile {
    computer_name  = "docker-${lookup(var.docker_nodes, count.index)}-vm"
    admin_username = "${var.ssh_user}"
  }

  os_profile_linux_config {
    disable_password_authentication = true
    ssh_keys {
      path     = "/home/${var.ssh_user}/.ssh/authorized_keys"
      key_data = "${file(var.ssh_public_key_path)}"
    }
  }

  provisioner "file" {
    content     = "${templatefile("../_files/bootstrap.sh", { docker-ee-url = var.docker_url, username = var.ssh_user })}"
    destination = "/tmp/bootstrap.sh"
  }

  provisioner "file" {
    content     = "${data.template_file.azure_json.rendered}"
    destination = "/tmp/azure.json"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo chmod a+x /tmp/bootstrap.sh",
      "sudo bash /tmp/bootstrap.sh",
      "sudo mkdir -p /etc/kubernetes",
      "sudo mv /tmp/azure.json /etc/kubernetes/azure.json",
      "sudo chown root: /etc/kubernetes/azure.json",
      "sudo chmod 0644 /etc/kubernetes/azure.json",
      "sudo rm -rf /tmp/*.sh",
      "sudo rm -rf /tmp/*.json",
    ]
  }

  connection {
    type         = "ssh"
    bastion_host = "${data.azurerm_public_ip.docker-jb-ip.fqdn}"
    host         = "${element(azurerm_network_interface.docker-node-nic.*.private_ip_address, count.index)}"
    user         = "${var.ssh_user}"
    private_key  = "${file(var.ssh_private_key_path)}"
  }
}

resource "null_resource" "bastion_ssh_config" {
  provisioner "file" {
    content     = "${data.template_file.ssh_config.rendered}"
    destination = "/home/${var.ssh_user}/.ssh/config"
  }

  connection {
    type        = "ssh"
    host        = "${data.azurerm_public_ip.docker-jb-ip.fqdn}"
    user        = "${var.ssh_user}"
    private_key = "${file(var.ssh_private_key_path)}"
  }
}
